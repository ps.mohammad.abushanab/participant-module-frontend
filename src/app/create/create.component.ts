import { Component } from '@angular/core';
import { UserService } from '../user/user.service';
import { User } from '../user/user.interface';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrl: './create.component.css'
})
export class CreateComponent {

  constructor(private userService: UserService){}

  createUser(userData: User): void {
    this.userService.createUser(userData).subscribe(
      (response) => {
        alert('Participant created');
      },
      (error) => {
        alert('Error creating participant');
      }
    );
  }

}
