import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { User } from "./user.interface";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: 'root'
})
export class UserService{

    private baseUrl = 'http://localhost:8080/api/participants';

    constructor(private http: HttpClient) { }
  
    getUsers(): Observable<User[]> {
      return this.http.get<User[]>(this.baseUrl);

    }
  
    getUserByCode(code: string): Observable<User> {
      return this.http.get<User>(`${this.baseUrl}/${code}`);
    }
  
    createUser(user: User): Observable<User> {
      return this.http.post<User>(this.baseUrl, user);
    }
  
    updateUser(code: string, user: User): Observable<User> {
      return this.http.put<User>(`${this.baseUrl}/${code}`, user);
    }
  
    deleteUser(code: string): Observable<void> {
      return this.http.delete<void>(`${this.baseUrl}/${code}`);
    }
}