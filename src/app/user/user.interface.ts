export interface User {
    id:number;
    code: number;
    bic: string;
    name: string;
    shortName: string;
    type: 'Direct' | 'Indirect';
    logo?: string; 
    settlementBank?: number;
}