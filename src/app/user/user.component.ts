import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { User } from './user.interface';
import { UserService } from './user.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrl: './user.component.css'
})
export class UserComponent implements OnInit {

  userForm: FormGroup;
  logoPreview: string | ArrayBuffer | null = null;
  @Output() formSubmit = new EventEmitter<User>();
  @Output() resetForm: EventEmitter<void> = new EventEmitter<void>();
  @Input() userToUpdate: User | null = null;
  showSettlementBank: boolean = false;


  constructor(private fb: FormBuilder, private userService: UserService) {
    this.userForm = this.fb.group({
      code: new FormControl('', [Validators.required, Validators.minLength(6), Validators.maxLength(6)]),
      bic: this.fb.group({
        bic1: new FormControl('', [Validators.required, Validators.pattern('^[A-Za-z]{4}$')]),
        bic2: new FormControl('', [Validators.required, Validators.pattern('^[A-Za-z]{2}$')]),
        bic3: new FormControl('', [Validators.required, Validators.pattern('^[A-Za-z0-9]{2}$')]),
        bic4: new FormControl('', [Validators.required, Validators.pattern('^[A-Za-z0-9]{3}$')]),
      }),
      name: new FormControl('', [Validators.required, Validators.pattern('^[A-Za-z ]+$')]),
      shortName: new FormControl('', [Validators.required, Validators.pattern('^[A-Za-z ]+$')]),
      type: new FormControl('', Validators.required),
      logo: new FormControl(''),
      settlementBank: new FormControl('')
    });

    
  }

  ngOnInit(): void {
    if (this.userToUpdate) {
      this.userForm.patchValue({
        id: this.userToUpdate.id,
        code: this.userToUpdate.code,
        name: this.userToUpdate.name,
        shortName: this.userToUpdate.shortName,
        type: this.userToUpdate.type,
        logo: this.userToUpdate.logo,
        settlementBank: this.userToUpdate.settlementBank
      });

     

      this.userForm.get('bic')!.patchValue({
        bic1: this.userToUpdate.bic.substring(0, 4),
        bic2: this.userToUpdate.bic.substring(4, 6),
        bic3: this.userToUpdate.bic.substring(6, 8),
        bic4: this.userToUpdate.bic.substring(8, 11)
      });

      console.log(this.userForm.get('type')!.value );
      if(this.userForm.get('type')!.value === "indirect"){
        this.toggleSettlementBank(true);
      }
      


    }

  }

  onSubmit(): void {
    if (this.userForm.valid) {
      const formData = this.userForm.value;
      formData.bic = this.getCombinedBic();
      if (this.userToUpdate) {
        formData.code = this.userToUpdate.code;
        this.formSubmit.emit(formData);
      } else {
        this.formSubmit.emit(formData);
      }
        this.userForm.reset();
    
    }else{
      alert("data is not valid");
    }
  }

  onLogoSelected(event: Event): void {
    const inputElement = event.target as HTMLInputElement;
    if (inputElement.files && inputElement.files[0]) {
      const reader = new FileReader();
      reader.onload = () => {
        this.logoPreview = reader.result;
      };
      reader.readAsDataURL(inputElement.files[0]);
    }
  }

  toggleSettlementBank(show: boolean) {
    this.showSettlementBank = show;
    if (!show) {
      this.userForm.get('settlementBank')?.setValue('');
    }
  }

  getCombinedBic(): string {
    const bicForm = this.userForm.get('bic')!;
    const bic1 = bicForm.get('bic1')!.value;
    const bic2 = bicForm.get('bic2')!.value;
    const bic3 = bicForm.get('bic3')!.value;
    const bic4 = bicForm.get('bic4')!.value;
    return `${bic1}${bic2}${bic3}${bic4}`;
  }



}
