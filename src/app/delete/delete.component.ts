import { Component, Input } from '@angular/core';
import { UserService } from '../user/user.service';

@Component({
  selector: 'app-delete',
  templateUrl: './delete.component.html',
  styleUrl: './delete.component.css'
})
export class DeleteComponent {

  @Input() code!: string;

  constructor(private userService: UserService) {}

  deleteUser(): void{
    this.userService.deleteUser(this.code).subscribe(
      (response) => {
        alert('Participant deleted successfully!');
      },
      (error) => {
        alert('Error deleted participant:' + error);
      }
    );
  }


}
