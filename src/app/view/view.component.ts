import { Component, OnInit } from '@angular/core';
import { User } from '../user/user.interface';
import { UserService } from '../user/user.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrl: './view.component.css'
})
export class ViewComponent implements OnInit {

  usersData: User[] = [];

  constructor(private userService: UserService) { }
  ngOnInit(): void {
     this.userService.getUsers().subscribe(data => {
      this.usersData = data;
     })
  }

}
