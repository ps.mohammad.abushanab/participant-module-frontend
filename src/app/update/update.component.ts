import { Component, Input, ViewChild } from '@angular/core';
import { UserService } from '../user/user.service';
import { User } from '../user/user.interface';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-update',
  templateUrl: './update.component.html',
  styleUrl: './update.component.css'
})
export class UpdateComponent {


  @Input() code!: string;
  userData!: User; 
  formSubmitted: boolean = false;
  @ViewChild('userForm') userForm!: NgForm;


  constructor(private userService: UserService) {}

  getUserData(): void {

    this.userService.getUserByCode(this.code).subscribe(
      (data) => {
        this.userData = data;
      },
      (error) => {
        alert('Error fetching user data:' + error);
      }
    );
    this.formSubmitted = false;
  }
  updateUser(userData:User): void{

    if (!this.userData) {
      alert('Participant to update not provided.');
      return;
    }
    this.userService.updateUser(this.code, userData).subscribe(
      (response) => {
        alert('Participant updated ');
      },
      (error) => {
        alert('Error updating participant');
      }
    );
    this.formSubmitted = true;
  }

  resetForm(): void {
    this.userForm.resetForm(); 
    this.formSubmitted = true;
  }


}
